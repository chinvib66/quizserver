<?php


header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
class Database{
 
    // specify your own database credentials
    private $host = "localhost";
    private $db_name = "test";
    private $username = "root";
    private $password = "";
    public $conn;
 
    // get the database connection
    public function getConnection(){
 
        $this->conn = null;
 
        try{
            $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username, $this->password);
            $this->conn->exec("set names utf8");
        }catch(PDOException $exception){
            echo "Connection error: " . $exception->getMessage();
        }
 
        return $this->conn;
    }
}

class User{

    private $conn;
    private $table_name = "quizuserdata";
 
    public $id;
    public $name;
    public $email;
    public $age;
    public $gender;
    public $address;
    public $qualification;
    public $profession;
    public $ip;
 
    public function __construct($db){
        $this->conn = $db;
    }

    function create(){
 
        // query to insert record
        $query = "INSERT INTO
                    " . $this->table_name . "
                SET
                    name=:name, email=:email, age=:age, gender=:gender, address=:address, qualification=:qualification, profession=:profession, ip=:ip";
     
        // prepare query
        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $this->name=htmlspecialchars(strip_tags($this->name));
        $this->email=htmlspecialchars(strip_tags($this->email));
        $this->age=htmlspecialchars(strip_tags($this->age));
        $this->gender=htmlspecialchars(strip_tags($this->gender));
        $this->address=htmlspecialchars(strip_tags($this->address));
        $this->qualification=htmlspecialchars(strip_tags($this->qualification));
        $this->profession=htmlspecialchars(strip_tags($this->profession));
        $this->ip=htmlspecialchars(strip_tags($this->ip));
     
        // bind values
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":email", $this->email);
        $stmt->bindParam(":age", $this->age);
        $stmt->bindParam(":gender", $this->gender);
        $stmt->bindParam(":address", $this->address);
        $stmt->bindParam(":qualification", $this->qualification);
        $stmt->bindParam(":profession", $this->profession);
        $stmt->bindParam(":ip", $this->ip);

        // execute query
        if($stmt->execute()){
            return true;
        }
        return false;
    }
}

class QuizAnswers{

    private $conn;
    private $table_name = "quizanswers";
 
    public $id;
    public $userid;
    public $answers;
 
    public function __construct($db){
        $this->conn = $db;
        $this->answers['1'] = "";
        $this->answers['2'] = "";
        $this->answers['3'] = "";
        $this->answers['4'] = "";
        $this->answers['5'] = "";
        $this->answers['6'] = "";
        $this->answers['7'] = "";
        $this->answers['8'] = "";
        $this->answers['9'] = "";
        $this->answers['10'] = "";
    }

    function create(){
 
        // query to insert record
        $query = "INSERT INTO
                    " . $this->table_name . "
                SET
                    userid=:userid, a1=:a1, a2=:a2, a3=:a3, a4=:a4, a5=:a5, a6=:a6, a7=:a7, a8=:a8, a9=:a9, a10=:a10";
     
        // prepare query
        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $this->userid=htmlspecialchars(strip_tags($this->userid));
     
        // bind values
        $stmt->bindParam(":userid", $this->userid);
        $stmt->bindParam(":a1", $this->answers['1']);
        $stmt->bindParam(":a2", $this->answers['2']);
        $stmt->bindParam(":a3", $this->answers['3']);
        $stmt->bindParam(":a4", $this->answers['4']);
        $stmt->bindParam(":a5", $this->answers['5']);
        $stmt->bindParam(":a6", $this->answers['6']);
        $stmt->bindParam(":a7", $this->answers['7']);
        $stmt->bindParam(":a8", $this->answers['8']);
        $stmt->bindParam(":a9", $this->answers['9']);
        $stmt->bindParam(":a10", $this->answers['10']);

        // execute query
        if($stmt->execute()){
            return true;
        }
        return false;
    }
}
?>