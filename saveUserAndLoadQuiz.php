
       <?php 

include 'config.php';

$database = new Database();
$db = $database->getConnection();

$userdata = new User($db);
$headers = apache_request_headers();
$data = json_decode(file_get_contents("php://input"));
if(
    !empty($data->name)&&
    !empty($data->email) &&
    !empty($data->age) &&
    !empty($data->gender) &&
    !empty($data->qualification) &&
    !empty($data->profession)
){
    $userdata->name = $data->name;
    $userdata->email = $data->email;
    $userdata->age = $data->age;
    $userdata->gender = $data->gender;
    $userdata->address = $data->address;
    $userdata->qualification = $data->qualification;
    $userdata->profession = $data->profession;
    $userdata->ip = $headers['Origin'];
    if($userdata->create()){
        http_response_code(201);
        $userid = $db->lastInsertId();
        $quiz = [
                    [
                        "question"=>[
                            "id"=>1, 
                            "value"=>"This is Question 1 with animation source",
                            "animation"=>"img/book.gif"
                        ], "options"=>[
                                [ "id"=>1, "value"=> "1 Option 1" ],
                                [ "id"=>2, "value"=> "1 Option 2" ],
                                [ "id"=>3, "value"=> "1 Option 3" ],
                                [ "id"=>4, "value"=> "1 Option 4" ],
                        ]                
                    ],
                    [
                        "question"=>[
                            "id"=>2, 
                            "value"=>"This is Question 2 with animation source",
                            "animation"=>"img/interview.gif"
                        ], "options"=>[
                                [ "id"=>1, "value"=> "2 Option 1" ],
                                [ "id"=>2, "value"=> "2 Option 2" ],
                                [ "id"=>3, "value"=> "2 Option 3" ],
                                [ "id"=>4, "value"=> "2 Option 4" ],
                        ],
                    ],
                    [
                        "question"=>[
                            "id"=>3, 
                            "value"=>"This is Question 3 with animation source",
                            "animation"=>"img/plan.gif"
                        ], "options"=>[
                                [ "id"=>1, "value"=> "3 Option 1" ],
                                [ "id"=>2, "value"=> "3 Option 2" ],
                                [ "id"=>3, "value"=> "3 Option 3" ],
                                [ "id"=>4, "value"=> "3 Option 4" ],
                        ]                
                    ],
                    [
                        "question"=>[
                            "id"=>4, 
                            "value"=>"This is Question 4 with animation source",
                            "animation"=>"img/sin.gif"
                        ], "options"=>[
                                [ "id"=>1, "value"=> "4 Option 1" ],
                                [ "id"=>2, "value"=> "4 Option 2" ],
                                [ "id"=>3, "value"=> "4 Option 3" ],
                                [ "id"=>4, "value"=> "4 Option 4" ],
                        ],
                    ],
                    [
                        "question"=>[
                            "id"=>5, 
                            "value"=>"This is Question 5 with animation source",
                            "animation"=>"img/book.gif"
                        ], "options"=>[
                                [ "id"=>1, "value"=> "5 Option 1" ],
                                [ "id"=>2, "value"=> "5 Option 2" ],
                                [ "id"=>3, "value"=> "5 Option 3" ],
                                [ "id"=>4, "value"=> "5 Option 4" ],
                        ]                
                    ],
                    [
                        "question"=>[
                            "id"=>6, 
                            "value"=>"This is Question 6 with animation source",
                            "animation"=>"img/cir.gif"
                        ], "options"=>[
                                [ "id"=>1, "value"=> "6 Option 1" ],
                                [ "id"=>2, "value"=> "6 Option 2" ],
                                [ "id"=>3, "value"=> "6 Option 3" ],
                                [ "id"=>4, "value"=> "6 Option 4" ],
                        ],
                    ],
                    [
                        "question"=>[
                            "id"=>7, 
                            "value"=>"This is Question 7 with animation source",
                            "animation"=>"img/waitingmovie.gif"
                        ], "options"=>[
                                [ "id"=>1, "value"=> "7 Option 1" ],
                                [ "id"=>2, "value"=> "7 Option 2" ],
                                [ "id"=>3, "value"=> "7 Option 3" ],
                                [ "id"=>4, "value"=> "7 Option 4" ],
                        ]                
                    ],
                    [
                        "question"=>[
                            "id"=>8, 
                            "value"=>"This is Question 8 with animation source",
                            "animation"=>"img/ear.gif"
                        ], "options"=>[
                                [ "id"=>1, "value"=> "8 Option 1" ],
                                [ "id"=>2, "value"=> "8 Option 2" ],
                                [ "id"=>3, "value"=> "8 Option 3" ],
                                [ "id"=>4, "value"=> "8 Option 4" ],
                        ],
                    ],
                    [
                        "question"=>[
                            "id"=>9, 
                            "value"=>"This is Question 9 with animation source",
                            "animation"=>"img/cir.gif"
                        ], "options"=>[
                                [ "id"=>1, "value"=> "9 Option 1" ],
                                [ "id"=>2, "value"=> "9 Option 2" ],
                                [ "id"=>3, "value"=> "9 Option 3" ],
                                [ "id"=>4, "value"=> "9 Option 4" ],
                        ]                
                    ],
                    [
                        "question"=>[
                            "id"=>10, 
                            "value"=>"This is Question 10 with animation source",
                            "animation"=>"img/interview.gif"
                        ], "options"=>[
                                [ "id"=>1, "value"=> "10 Option 1" ],
                                [ "id"=>2, "value"=> "10 Option 2" ],
                                [ "id"=>3, "value"=> "10 Option 3" ],
                                [ "id"=>4, "value"=> "10 Option 4" ],
                        ],
                    ],
                    
                ];


        echo json_encode(array("status"=>true, "userid"=>$userid, "quiz"=> $quiz));

    }
    else{
        http_response_code(503);
        echo json_encode(array("message" => "Error", "status"=>false));
    }
}
else{
    http_response_code(400);
    echo json_encode(array("message" => "Data is incomplete.", "status"=>false));
}
?>
