<?php 

    include 'config.php';
    
    $database = new Database();
    $db = $database->getConnection();
    
    $quiz = new QuizAnswers($db);
    $headers = apache_request_headers();

    $data = json_decode(file_get_contents("php://input"));
    if(
        !empty($data->userid)
    ){
        $quiz->userid = $data->userid;

        $answers = $data->answers;
        foreach($answers as $ans){
            $q = $ans->question;
            $a = $ans->answer;
            $quiz->answers[$q]=$a;
        }
        echo json_encode($quiz);
        
        if($quiz->create()){
            http_response_code(201);
            echo json_encode(array("status"=>true, "res"=>$db->errorInfo()));
        }
        else{
            http_response_code(503);
            echo json_encode(array("message" => "Error", "res"=>$db->errorInfo()));
        }
    }
    else{
        http_response_code(400);
        echo json_encode(array("message" => "Data is incomplete.", "status"=>false));
    }


?>
